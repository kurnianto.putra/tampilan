import React from "react";
import ReactDOM from "react-dom";
import Carousel from "./carousel";
import Dots from "./indicator-dots";
import Buttons from "./buttons";
import Fatimah from "./KamarView/Fatimah";
import Khadijah from "./KamarView/Khadijah";
import "./KamarView/kamar.css";
import Waktu from "./KamarView/waktu";
// Main App
class App extends React.Component {
  constructor() {
    super();
    this.state = {
      axis: "x"
    };
    this.setAxis = axis => {
      return () => this.setState({ axis: axis });
    };
  }
  render() {
    return (
      <div style={{ height: "100%" }}>
        <div>
          <h4>Informasi Ter-Update Kamar Rawatan RSIA ZAINAB</h4>
        </div>
        <div className="header">
          <ul>
            <li style={{ color: "green" }}>
              <strong>Warna Hijau Bertanda Ruangan Kosong</strong>
            </li>
            <li style={{ color: "red" }}>
              <strong>Warna Merah Bertanda Ruangan Terisi</strong>
            </li>
            <li style={{ color: "#FCD12A" }}>
              <strong>Warna Kuning Bertanda Ruangan Sedang Dibersihkan</strong>
            </li>
            <li style={{ color: "#4880ff" }}>
              <strong>Warna biru Bertanda Ruangan Sedang Diperbaiki</strong>
            </li>
          </ul>
          <Waktu />
        </div>

        <Carousel
          loop
          auto
          axis={this.state.axis}
          widgets={[Dots, Buttons]}
          className="custom-class"
        >
          <div>
            <Fatimah />
          </div>
          <div>
            <Khadijah />
          </div>
          {/* <p style={{backgroundColor: 'royalblue', height: '100%'}}>FRAME 1</p>
          <p style={{backgroundColor: 'orange', height: '100%'}}>FRAME 2</p>
          <p style={{backgroundColor: 'orchid', height: '100%'}}>FRAME 3</p> */}
        </Carousel>
      </div>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("root"));
