import React, { Component } from "react";
import { apiURL } from "./api";
import axios from "axios";

class Khadijah extends Component {
  constructor() {
    super();
    this.state = {
      kamartersedia: []
    };
  }

  componentWillMount() {
    setInterval(this.kamartersedia, 1000);
  }

  kamartersedia = () => {
    axios.get(apiURL + "/kamar/khadijah").then(ambildata => {
      this.setState({ kamartersedia: ambildata.data });
    });
  };

  renderKhadijah() {
    return this.state.kamartersedia.map((item, index) => (
      <li
        className="flex-item"
        style={{
          backgroundColor:
            item.id_status_kamar === 1
              ? "green"
              : item.id_status_kamar === 2
              ? "red"
              : item.id_status_kamar === 3
              ? "#FCD12A"
              : "blue"
        }}
        key={index}
      >
        <div> {item.tipe} </div>
        <div> {item.nomor_kamar} </div>
      </li>
    ));
  }

  render() {
    return (
      <div className="card">
        <div className="card-header">
          <h5>
            <i className="fa fa-bed" /> Lantai 3 Ruang Khadijah{" "}
          </h5>
        </div>
        <ul className="flex-container wrap-reverse">{this.renderKhadijah()}</ul>
      </div>
    );
  }
}
export default Khadijah;
