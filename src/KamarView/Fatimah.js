import React, { Component } from "react";
import { apiURL } from "./api";
import axios from "axios";

class Fathimah extends Component {
  constructor() {
    super();
    this.state = {
      kamartersedia: []
    };
  }

  componentWillMount() {
    setInterval(this.kamartersedia, 1000);
  }
  kamartersedia = () => {
    axios.get(apiURL + "/kamar/fatimah").then(ambildata => {
      this.setState({ kamartersedia: ambildata.data });
    });
  };
  renderFathimah() {
    return this.state.kamartersedia.map((item, index) => (
      <li
        className="flex-item"
        style={{
          backgroundColor:
            item.id_status_kamar === 1
              ? "green"
              : item.id_status_kamar === 2
              ? "red"
              : item.id_status_kamar === 3
              ? "#FCD12A"
              : "blue"
        }}
        key={index}
      >
        <div> {item.tipe} </div>
        <div> {item.nomor_kamar} </div>
      </li>
    ));
  }

  render() {
    const { kamartersedia } = this.state;

    return (
      <div className="card">
        <div className="card-header">
          <h5>
            <i className="fa fa-bed" /> Lantai 4 Ruang Fathimah{" "}
          </h5>
        </div>
        <ul className="flex-container wrap-reverse">{this.renderFathimah()}</ul>
      </div>
    );
  }
}
export default Fathimah;
