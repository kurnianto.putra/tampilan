import React, { Component } from "react";

export default class Waktu extends Component {
  constructor() {
    super();
    let d = new Date();
    this.state = {
      day: d.getDay(),
      month: d.getMonth(),
      date: d.getDate(),
      year: d.getFullYear(),
      time: d.toLocaleTimeString()
    };
    this.hitungDetik = this.hitungDetik.bind(this);
  }

  hitungDetik() {
    let d = new Date();
    this.setState({
      day: d.getDay(),
      month: d.getMonth(),
      date: d.getDate(),
      year: d.getFullYear(),
      time: d.toLocaleTimeString()
    });
  }
  componentWillMount() {
    setInterval(this.hitungDetik, 1000);
  }
  render() {
    const months = [
      "January",
      "February",
      "Maret",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "November",
      "December"
    ];
    const days = [
      "Minggu",
      "Senin",
      "Selasa",
      "Rabu",
      "Kamis",
      "Jum'at",
      "Sabtu"
    ];
    return (
      <div className="waktuSekarang">
        <i className="fa fa-clock" /> {days[this.state.day]} - {this.state.date}{" "}
        {months[this.state.month]} {this.state.year} - {this.state.time}
      </div>
    );
  }
}
